# Custom RuboCop Cop

A tutorial/example of how to create your own RuboCop Cop because for every Ruby project you may have your own custom linting rules that you want to enforce.

Based on the example of [Lint/RequireParentheses](https://github.com/bbatsov/rubocop/blob/master/lib/rubocop/cop/lint/require_parentheses.rb)

Copyright (C) 2017 Rudolf Olah

Licensed under the MIT license
